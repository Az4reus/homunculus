name := "Homunculus"

version := "0.1"

scalaVersion := "2.11.7"

// Discord4J
resolvers += "bintray-austinv11-maven" at "http://dl.bintray.com/austinv11/maven"

// pizza-eveapi
resolvers += Resolver.jcenterRepo

libraryDependencies ++= Seq(
  "sx.blah"                     % "Discord4J"     % "2.0.2",
  "org.scalaj"                 %% "scalaj-http"   % "2.2.1",
  "moe.pizza"                  %% "eveapi"        % "0.25",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
)

// Test things.
// =============================================================================
libraryDependencies ++= Seq(
 "org.scalatest" %% "scalatest"   % "2.2.6"   % "test",
 "org.mockito"   % "mockito-core" % "1.10.19" % "test"
)
