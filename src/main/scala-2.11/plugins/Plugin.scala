package plugins

import sx.blah.discord.handle.obj.{Channel, Message}

/**
  * Created by az on 12/01/16.
  */
trait Plugin {
  def apply(message: Message)

  def help(channel: Channel)

  def init()
}
