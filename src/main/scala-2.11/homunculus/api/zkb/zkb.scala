package homunculus.api.zkb

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import homunculus.Types.Eve.CharacterID
import homunculus.Types.Zkb.CharacterStatistics

import scalaj.http.Http

/**
  * Created by az on 11/01/16.
  */
object Zkb {
  val OM = new ObjectMapper()
  OM.registerModule(DefaultScalaModule)


  def requestCharacterStats(id: CharacterID): CharacterStatistics = {
    val queryResult = zkbRequest(s"https://zkillboard.com/api/stats/characterID/${id.ID}/")
    OM.readValue(queryResult.asString.body, classOf[CharacterStatistics])
  }


  def zkbRequest(url: String) = {
    // TODO: Add proper headers, like a user-agent and a gzip header.
    Http(url)
  }
}
