package homunculus.EventHandlers

import com.typesafe.scalalogging.{LazyLogging, Logger}
import homunculus.plugins.{Who, Hello}
import homunculus.util.ImplicitHelpers._
import org.slf4j.LoggerFactory
import sx.blah.discord.handle.IListener
import sx.blah.discord.handle.impl.events.MessageReceivedEvent

/**
  * Created by az on 11/01/16.
  */
class MessageReceivedHandler extends IListener[MessageReceivedEvent] with LazyLogging {

  /**
    * This is the on-message dispatcher, working much the same as the one in Python did.
    *
    * @param event The event that is pushed to us when a message is sent in the client.
    */
  override def handle(event: MessageReceivedEvent) = {
    val message = event.getMessage
    val c: String = message.getContent

    logger.info(s"received message '$c'")

    // 'prefix % string' is directly translated to 'string.startsWith(prefix)',
    // It is only used here for the ease of reading compared to the long form.
    c match {
      case hello if c.startsWith("!hello") => Hello(message)
      case who   if c.startsWith("!who ")  => Who(message)
      case _ =>
    }
  }
}
