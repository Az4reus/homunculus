package homunculus.Types.Eve

/**
  * Created by az on 18/01/16.
  */
sealed trait EntityID

final case class CharacterID(ID: Long) extends EntityID
final case class CorporationID(ID: Long) extends EntityID
final case class AllianceID(ID: Long) extends EntityID
