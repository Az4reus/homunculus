package homunculus.Types.Zkb

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}

/**
  * Created by az on 18/01/16.
  */

@JsonIgnoreProperties(ignoreUnknown = true)
case class CharacterStatistics(id: Long,
                               shipsLost: Int,
                               pointsLost: Int,
                               iskLost: Long,
                               groups: Map[Int, CharStatsGroup],
                               months: Map[Int, CharStatsMonth],
                               shipsDestroyed: Int,
                               pointsDestroyed: Int,
                               iskDestroyed: Long,
                               sequence: Int,
                               info: CharStatsInfo)

case class CharStatsInfo(allianceID: Long,
                         corporationID: Long,
                         factionID: Long,
                         id: Long,
                         killID: Long,
                         lastApiUpdate: Map[String, Long],
                         name: String,
                         @JsonProperty("type") kind: String)

case class CharStatsGroup(groupID: Int,
                          shipsLost: Int,
                          pointsLost: Int,
                          iskLost: Long,
                          shipsDestroyed: Int,
                          pointsDestroyed: Int,
                          iskDestroyed: Long)


case class CharStatsMonth(year: Int,
                          month: Int,
                          shipsLost: Int,
                          pointsLost: Int,
                          iskLost: Long,
                          shipsDestroyed: Int,
                          pointsDestroyed: Int,
                          iskDestroyed: Long)