package homunculus

import homunculus.EventHandlers.MessageReceivedHandler
import sx.blah.discord.api._

/**
  * Created by az on 11/01/16.
  */
class Homunculus(val user: String, val password: String) {
  val client = new ClientBuilder().withLogin(user, password).login()

  println("Homunculus instantiated")

  client.getDispatcher
    .registerListener(new MessageReceivedHandler())
}
