package homunculus.plugins

import sx.blah.discord.handle.obj.Message
import homunculus.util.ImplicitHelpers._

/**
  * Simple proof of concept command handler, has a static resonse to a given command.
  * This is very likely to get replaced in the future
  */
object Hello {
  def apply(message: Message) = {
    message post "Hello peasant. I am the newer and shinier Homunculus. Pray to me for forgiveness."
  }
}
