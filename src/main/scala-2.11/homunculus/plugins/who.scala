package homunculus.plugins

import com.typesafe.scalalogging.{LazyLogging, Logger}
import homunculus.Types.Eve.CharacterID

import scala.concurrent.ExecutionContext.Implicits.global

import homunculus.api.zkb.Zkb
import moe.pizza.eveapi.{XMLApiResponse, ApiKey, EVEAPI}
import sx.blah.discord.handle.obj.Message
import util.ImplicitHelpers._

/**
  * Created by az on 18/01/16.
  */
object Who extends LazyLogging {

  implicit val apiKey = None
  val eveApi = new EVEAPI()

  def apply(message: Message): Unit = {
    val c = message.getContent
    val term = c.substring(5)
    logger.info(s"Received message '$term'")

    val response = makeResponse(term)
    message post response
  }


  def makeResponse(term: String): String = {
    term match {
      // Default, no-name-given quip goes here.
      case "" => "\nI am grateful I cannot read your mind, but do tell me who you want to find."
      case _ =>
        eveApi.eve.CharacterID(List(term))
          .sync()
          .map(_.result) match {

          // if this fails, either the API is down, or we can't connect to it, meaning Bad Things.
          case xs if xs.isFailure => "Shit's on fire, oooorr, the API is down. Take your pick."
          case xs => xs.get
            .map(_.characterID)
            .map(_.toLong)
            .map { charID =>

              // This (return ID being 0) is the error case for the API, anything that is an
              // actual error indicates that the API is currently fucked.
              charID match {
                case 0 => "\nDid not find your dude.\nEnter a dude who's bad enough to be found."
                case _ =>

                  val zkbInfo = Zkb.requestCharacterStats(CharacterID(charID))

                  val name = zkbInfo.info.name
                  val kills = zkbInfo.shipsDestroyed
                  val losses = zkbInfo.shipsLost

                  val corpID = zkbInfo.info.corporationID
                  val allianceID = zkbInfo.info.allianceID

                  val nameRequest = eveApi.eve.CharacterName(List(corpID, allianceID))
                    .sync()
                    .map(_.result)
                    .getOrElse(Seq.empty)

                  // a character can't not have a corporation, so we can just omit checking that.
                  val corp = nameRequest.find(e => e.characterID.toLong == corpID).get.name

                  val allianceName = nameRequest.find(_.characterID.toLong == allianceID).get.name
                  val alliance = if (allianceName == "Unknown Item") "*<No Alliance>*" else allianceName

                  s"""
                    |$name [$kills|| $losses ]
                    |$corp
                    |$alliance
                  """.stripMargin
              }
            }.head
        }
    }
  }
}
