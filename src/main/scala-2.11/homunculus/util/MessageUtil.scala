package homunculus.util

import sx.blah.discord.handle.obj.Message

/**
  * Created by az on 12/01/16.
  */

object ImplicitHelpers {

  implicit class MessageUtil(m: Message) {
    def post(msg: String): Unit = {
      m.getChannel.sendMessage(msg)
    }
  }

  implicit class StringPattern(s: String) {
    def %(content: String): Boolean = {
      s.startsWith(content)
    }
  }

}
