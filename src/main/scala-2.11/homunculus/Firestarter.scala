package homunculus

/**
  * Created by az on 11/01/16.
  */
object Firestarter extends App{
  override def main (args: Array[String]) {

    val split = io.Source.fromFile("account").mkString.split(" ")
    val name = split(0)
    val password = split(1)

    new Homunculus(name, password)
  }
}
