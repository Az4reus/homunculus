package util

import sx.blah.discord.handle.obj.Message

/**
  * Created by az on 12/01/16.
  */

object ImplicitHelpers {

  implicit class MessagePostUtil(m: Message) {
    def post(msg: String): Unit = {
      m.getChannel.sendMessage(msg)
    }
  }

  implicit class StringStartsWith(s: String) {
    def %(content: String): Boolean = {
      s.startsWith(content)
    }
  }

}
