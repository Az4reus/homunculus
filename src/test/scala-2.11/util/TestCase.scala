package util

import org.scalatest.mock.MockitoSugar
import org.scalatest.{FunSpec, Matchers}

/**
  * This is the basic test-case for our specs, with the RSpec like FunSpec used
  * as basis. The traits we use so far to customise that:
  *
  * Matchers:
  * This trait allows us to write natural-language assertions, like
  * "3 should equal (3)".
  */
class TestCase extends FunSpec
               with Matchers
               with MockitoSugar{}
