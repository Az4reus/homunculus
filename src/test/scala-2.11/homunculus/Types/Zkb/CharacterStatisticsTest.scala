package homunculus.Types.Zkb

import homunculus.Types.Eve.CharacterID
import homunculus.api.zkb.Zkb
import util.TestCase

/**
  * Created by az on 18/01/16.
  */
class CharacterStatisticsTest extends TestCase {
  describe("Test runs of the API") {

    it("should retrieve and package a characterStats request.") {

      val result: CharacterStatistics = Zkb.requestCharacterStats(new CharacterID(879040384))
      result.info.name should be ("Tyraez")
      result.id should be (879040384)
    }
  }
}
