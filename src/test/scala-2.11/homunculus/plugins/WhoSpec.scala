package homunculus.plugins

import util.TestCase

/**
  * Created by az on 18/01/16.
  */
class WhoSpec extends TestCase {

  describe("Error handling") {

    it("Should output error on no-args call") {
      val response = Who.makeResponse("")
      response should be ("\nI am grateful I cannot read your mind, but do tell me who you want to find.")
    }

    it("Should output error to gibberish input") {
      val response = Who.makeResponse("sofjoajsdasd")
      response should be ("\nDid not find your dude.\nEnter a dude who's bad enough to be found.")
    }
  }

  describe("Functionality") {
    it("Should find the right name to input.") {
        val response = Who.makeResponse("chribba")
        assert(response.contains("Chribba"))
    }

    it("Should find the correct corp and alliance to a valid name.") {
      val response = Who.makeResponse("chribba")
      assert(response.contains("Otherworld Enterprises"))
      assert(response.contains("Otherworld Empire"))
    }

    it("Should properly output for corps with no alliances.") {
      val response = Who.makeResponse("Brimtak")
      assert(response.contains("<No Alliance>"))
    }
  }
}
